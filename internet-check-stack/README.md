# Internet check stack
Stack for checking internet speed and ping.

## Prometheus config example
```
  - job_name: 'speedtest'
    metrics_path: /metrics
    scrape_interval: 30m
    scrape_timeout: 60s # running speedtest needs time to complete
    static_configs:
      - targets: ['speedtest:9798']

  - job_name: 'ping'
    metrics_path: /probe
    scrape_interval: 5s
    params:
      module: [http_2xx]  # Look for a HTTP 200 response.
    file_sd_configs:
      - files:
        - pinghosts.yaml
    relabel_configs:
      - source_labels: [__address__]
        regex: '(.*);(.*);(.*);(.*)'  #first is the url, thus unique for instance
        target_label: instance
        replacement: $1
      - source_labels: [__address__]
        regex: '(.*);(.*);(.*);(.*)'  #second is humanname to use in charts
        target_label: humanname
        replacement: $2
      - source_labels: [__address__]
        regex: '(.*);(.*);(.*);(.*)'  #third state whether this is testing external or internal network
        target_label: routing
        replacement: $3
      - source_labels: [__address__]
        regex: '(.*);(.*);(.*);(.*)'  #fourth is which switch/router the device is connected to.
        target_label: switching
        replacement: $4
      - source_labels: [instance]
        target_label: __param_target
      - target_label: __address__
        replacement: ping:9115  # The blackbox exporter's real hostname:port.
```

pinghosts.yaml file
```
- targets:  # url;humanname;routing;switch
    - http://www.google.com/;google.com;external;internetbox
    - https://github.com/;github.com;external;internetbox
    - https://www.youtube.com;youtube.com;external;internetbox
```
