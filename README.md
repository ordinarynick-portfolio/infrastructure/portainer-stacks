# Portainer stacks

Contains Docker Compose files and configurations for easy deployment to Portainer as a stack.

## How to use
Just clone this repository to /opt and copy-paste Docker Compose files to Portainer.

If you clone it to another directory, you must change paths to files in Docker Compose files. Also, you can edit the configuration to suit your needs.

## List of stacks

### Internet check stack
Docker compose (stack) for deploying speedtest-exporter with blackbox (ping checker) to check internet speed and ping.

### Jellyfin stack
Docker compose (stack) for deploying Jellyfin web application.

### Monitoring stack
Docker Compose (stack) for deploying Prometheus, AlertManager, and Grafana.
